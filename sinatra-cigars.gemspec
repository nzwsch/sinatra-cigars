require File.expand_path '../lib/sinatra/cigars/version', __FILE__

Gem::Specification.new do |spec|
  spec.author      = 'Seiichi Yonezawa'
  spec.description = ''
  spec.homepage    = 'http://sinatra-cigars.nzwsch.com'
  spec.name        = 'sinatra-cigars'
  spec.summary     = spec.description
  spec.version     = Sinatra::Cigars::VERSION

  spec.files         = `git ls-files -z`.split("\x0")
  spec.require_paths = ["lib"]

  spec.add_dependency "sinatra", "~> 1.4"
  spec.add_dependency "haml",    "~> 4.0"
end
