require 'sinatra/base'
require 'haml'

module Sinatra
  class Cigars < Base
    module Helpers
      def development?
        ENV['RACK_ENV'] == 'development'
      end

      def test?
        ENV['RACK_ENV'] == 'test'
      end

      def staging?
        ENV['RACK_ENV'] == 'staging'
      end

      def production?
        ENV['RACK_ENV'] == 'production'
      end

      def home?
        request.path == '/'
      end

      def meta arg
        arg.is_a?(Hash) ? meta_hash(arg) : meta_str(arg)
      end

      def css href = '/style'
        render_haml "%link{rel: 'stylesheet', href: '#{render_tag(href, '.css')}'}"
      end

      def js src
        render_haml "%script{src: '#{render_tag(src, '.js')}'}"
      end

      def favicon src = nil
        render_haml "%link{rel: 'shortcut icon', href: '#{render_tag((src ? src : '/favicon'), (src ? '.png' : '.ico'))}'}"
      end

      def title str, prefix = "", suffix = false
        home? ? render_haml("%title= '#{str}'") : render_haml("%title= '#{suffix ? str + prefix : prefix + str}'")
      end

      def livereload
        haml "%script{src: 'http://#{request.host}:35729/livereload.js'}" if development? or test?
      end

      def typekit id
        src = <<-EOF.gsub(/^ {8}/, '')
        <script src="//use.typekit.net/#{id}.js"></script>
        <script>try{Typekit.load();}catch(e){}</script>
        EOF
      end

      private

      def render_haml code
        Haml::Engine.new(code).render
      end

      def render_tag code, ext
        code = "/#{code}" unless code.is_a? String
        code = code + ext unless code.end_with? ext
        production? ? code + '?' + `git rev-parse --short HEAD`.chomp : code
      end

      def meta_hash hash
        a = hash.first
        render_haml "%meta{name: '#{a.first}', content: '#{a.last}'}"
      end

      def meta_str str
        case str.to_s
        when 'charset'  then render_haml "%meta{charset: 'utf-8'}"
        when 'edge'     then render_haml "%meta{'http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge'}"
        when 'viewport' then render_haml "%meta{name: 'viewport', content: 'width=device-width,initial-scale=1'}"
        end
      end
    end

    helpers Helpers
  end

  helpers Cigars::Helpers
end
