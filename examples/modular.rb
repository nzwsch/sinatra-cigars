$:.unshift 'lib'

require 'sinatra/cigars'

class App < Sinatra::Cigars
  get '/' do
    haml "= home? ? 'Hooray!' : 'Oops.'"
  end
end
