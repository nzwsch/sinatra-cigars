$:.unshift 'lib'

require 'sinatra'
require 'sinatra/cigars'

get '/' do
  haml "= home? ? 'Hooray!' : 'Oops.'"
end
