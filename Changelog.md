# Changelog

## 0.0.13 - 2014-08-03

  * Added `favicon` to helpers
  * Change Sinatra::Cigars to class

## 0.0.12 - 2014-05-05

  * Change `title` returns always "title" in home

## 0.0.11 - 2014-05-04

  * Added helpers (`home?`, `meta`, `title`)
  * Added haml to dependencies

## 0.0.10 - 2014-05-03

  * Added Sinatra::Cigars::Helpers
