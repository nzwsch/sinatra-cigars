ENV['RACK_ENV'] = 'test'

require 'capybara/rspec'
require 'sinatra/cigars'

module Sinatra
  module TestHelpers
    def mock_app &block
      @app = Sinatra.new Sinatra::Application do
        class_eval &block
      end
      Capybara.app = @app
    end
  end
end
