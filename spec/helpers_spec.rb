require 'spec_helper'

describe Sinatra::Cigars::Helpers, type: :feature do
  include Sinatra::TestHelpers

  describe 'development?' do
    before do
      ENV['RACK_ENV'] = 'development'

      mock_app do
        get('/development') { development?.to_s }
        get('/test')        { test?.to_s        }
        get('/staging')     { staging?.to_s     }
        get('/production')  { production?.to_s  }
      end
    end

    it do
      visit '/development'
      body.should == 'true'
    end

    it do
      visit '/test'
      body.should == 'false'
    end

    it do
      visit '/staging'
      body.should == 'false'
    end

    it do
      visit '/production'
      body.should == 'false'
    end
  end

  describe 'test?' do
    before do
      ENV['RACK_ENV'] = 'test'

      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/development') { development?.to_s }
        get('/test')        { test?.to_s        }
        get('/staging')     { staging?.to_s     }
        get('/production')  { production?.to_s  }
      end
    end

    it do
      visit '/development'
      body.should == 'false'
    end

    it do
      visit '/test'
      body.should == 'true'
    end

    it do
      visit '/staging'
      body.should == 'false'
    end

    it do
      visit '/production'
      body.should == 'false'
    end
  end

  describe 'staging?' do
    before do
      ENV['RACK_ENV'] = 'staging'

      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/development') { development?.to_s }
        get('/test')        { test?.to_s        }
        get('/staging')     { staging?.to_s     }
        get('/production')  { production?.to_s  }
      end
    end

    it do
      visit '/development'
      body.should == 'false'
    end

    it do
      visit '/test'
      body.should == 'false'
    end

    it do
      visit '/staging'
      body.should == 'true'
    end

    it do
      visit '/production'
      body.should == 'false'
    end
  end

  describe 'production?' do
    before do
      ENV['RACK_ENV'] = 'production'

      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/development') { development?.to_s }
        get('/test')        { test?.to_s        }
        get('/staging')     { staging?.to_s     }
        get('/production')  { production?.to_s  }
      end
    end

    it do
      visit '/development'
      body.should == 'false'
    end

    it do
      visit '/test'
      body.should == 'false'
    end

    it do
      visit '/staging'
      body.should == 'false'
    end

    it do
      visit '/production'
      body.should == 'true'
    end
  end

  describe 'css' do
    before do
      @rev = `git rev-parse --short HEAD`.chomp

      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/')       { haml '= css' }
        get('/symbol') { haml '= css :symbol' }
        get('/string') { haml '= css "string"' }
        get('/ext')    { haml '= css "/ext.css"' }
      end
    end

    it do
      ENV['RACK_ENV'] = 'test'
      visit '/'
      body.should == "<link href='/style.css' rel='stylesheet'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'production'
      visit '/'
      body.should == "<link href='/style.css?#{@rev}' rel='stylesheet'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'test'
      visit '/symbol'
      body.should == "<link href='/symbol.css' rel='stylesheet'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'production'
      visit '/symbol'
      body.should == "<link href='/symbol.css?#{@rev}' rel='stylesheet'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'test'
      visit '/string'
      body.should == "<link href='string.css' rel='stylesheet'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'production'
      visit '/string'
      body.should == "<link href='string.css?#{@rev}' rel='stylesheet'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'test'
      visit '/ext'
      body.should == "<link href='/ext.css' rel='stylesheet'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'production'
      visit '/ext'
      body.should == "<link href='/ext.css?#{@rev}' rel='stylesheet'>\n"
    end
  end

  describe 'js' do
    before do
      @rev = `git rev-parse --short HEAD`.chomp

      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/') { haml '= js "/script"' }
      end
    end

    it do
      ENV['RACK_ENV'] = 'test'
      visit '/'
      body.should == "<script src='/script.js'></script>\n"
    end

    it do
      ENV['RACK_ENV'] = 'production'
      visit '/'
      body.should == "<script src='/script.js?#{@rev}'></script>\n"
    end
  end

  describe 'favicon' do
    before do
      @rev = `git rev-parse --short HEAD`.chomp

      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/')     { haml '= favicon' }
        get('/path') { haml '= favicon :path' }
      end
    end

    it do
      ENV['RACK_ENV'] = 'test'
      visit '/'
      body.should == "<link href='/favicon.ico' rel='shortcut icon'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'production'
      visit '/'
      body.should == "<link href='/favicon.ico?#{@rev}' rel='shortcut icon'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'test'
      visit '/path'
      body.should == "<link href='/path.png' rel='shortcut icon'>\n"
    end

    it do
      ENV['RACK_ENV'] = 'production'
      visit '/path'
      body.should == "<link href='/path.png?#{@rev}' rel='shortcut icon'>\n"
    end
  end

  describe 'livereload' do
    before do
      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/development') { haml '= livereload' }
        get('/test')        { haml '= livereload' }
        get('/staging')     { haml '= livereload' }
        get('/production')  { haml '= livereload' }
      end
    end

    it do
      ENV['RACK_ENV'] = 'development'
      visit '/development'
      body.should == "<script src='http://www.example.com:35729/livereload.js'></script>\n"
    end

    it do
      ENV['RACK_ENV'] = 'test'
      visit '/test'
      body.should == "<script src='http://www.example.com:35729/livereload.js'></script>\n"
    end

    it do
      ENV['RACK_ENV'] = 'staging'
      visit '/staging'
      body.should == "\n"
    end

    it do
      ENV['RACK_ENV'] = 'production'
      visit '/production'
      body.should == "\n"
    end
  end

  describe 'typekit' do
    before do
      ENV['RACK_ENV'] = 'test'

      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/') { haml '= typekit :kitid' }
      end
    end

    it do
      visit '/'
      body.should == "<script src=\"//use.typekit.net/kitid.js\"></script>\n<script>try{Typekit.load();}catch(e){}</script>\n"
    end
  end

  describe 'home?' do
    before do
      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/')     { home?.to_s }
        get('/home') { home?.to_s }
      end
    end

    it do
      visit '/'
      body.should == 'true'
    end

    it 'should == "true" with params' do
      visit '/?foo=baz'
      body.should == 'true'
    end

    it do
      visit '/home'
      body.should == 'false'
    end
  end

  describe 'meta' do
    before do
      mock_app do
        helpers Sinatra::Cigars::Helpers

        get('/author')      { haml '= meta author: "John Q Public"' }
        get('/charset')     { haml '= meta :charset' }
        get('/description') { haml '= meta description: "page description"' }
        get('/edge')        { haml '= meta :edge' }
        get('/viewport')    { haml '= meta :viewport' }
      end
    end

    it do
      visit '/author'
      body.should == "<meta content='John Q Public' name='author'>\n"
    end

    it do
      visit '/charset'
      body.should == "<meta charset='utf-8'>\n"
    end

    it do
      visit '/description'
      body.should == "<meta content='page description' name='description'>\n"
    end

    it do
      visit '/edge'
      body.should == "<meta content='IE=edge' http-equiv='X-UA-Compatible'>\n"
    end

    it do
      visit '/viewport'
      body.should == "<meta content='width=device-width,initial-scale=1' name='viewport'>\n"
    end
  end

  describe 'title' do
    before do
      mock_app do
        helpers Sinatra::Cigars::Helpers

        helpers do
          def haml_title
            haml <<-EOF.gsub /^ {12}/, ''
            = title 'title'
            = title 'title', 'prefix - '
            = title 'title', ' - suffix', true
            EOF
          end
        end

        get('/')     { haml_title }
        get('/page') { haml_title }
      end
    end

    it 'returns always "title" at home' do
      visit '/'
      body.should == "<title>title</title>\n<title>title</title>\n<title>title</title>\n"
    end

    it do
      visit '/page'
      body.should == "<title>title</title>\n<title>prefix - title</title>\n<title>title - suffix</title>\n"
    end
  end
end
