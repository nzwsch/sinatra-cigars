require 'spec_helper'

describe Sinatra::Cigars do
  context 'VERSION' do
    it do
      Sinatra::Cigars::VERSION.should match /^\d+\.\d+\.\d+/
    end
  end

  context 'Helpers' do
    it 'included automatically' do
      defined?(Sinatra::Cigars::Helpers).should be_truthy
    end
  end
end
